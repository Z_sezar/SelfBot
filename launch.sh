#!/bin/bash


if pacman -Q &> /dev/null ;then
  if [ ! -f ./launch_arch.sh ]; then
    wget -O ./launch_arch.sh 'https://gitlab.com/BeatBotTeam/SelfBot/raw/master/launch_arch.sh' || curl -O  https://gitlab.com/BeatBotTeam/SelfBot/raw/master/launch_arch.sh
  fi
  sudo chmod +x ./launch_arch.sh
 ./launch_arch.sh
elif apt list --installed &> /dev/null ;then
  if [ ! -f ./launch_debian.sh ]; then
    wget -O ./launch_debian.sh 'https://gitlab.com/BeatBotTeam/SelfBot/raw/master/launch_debian.sh' || curl -O  https://gitlab.com/BeatBotTeam/SelfBot/raw/master/launch_debian.sh
  fi
  sudo chmod +x ./launch_debian.sh
  ./launch_debian.sh
else
      sudo chmod +x ./launch_fedora.sh
 ./launch_fedora.sh
fi
